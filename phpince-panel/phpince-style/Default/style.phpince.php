<?php
$PHPINCE_STYLE = array(
	"topic_style" => "<h1>{PHPINCE_TITLE} | {PHPINCE_DATE} - {PHPINCE_TIME}</h1><p>{PHPINCE_TEXT}</p><hr>",
	"topic_style_active" => "<h1>{PHPINCE_TITLE}</h1><p>{PHPINCE_TEXT}</p>",
	"topic_error" => "No topic writed",
	"topic_notfound" => "Sorry, topic not found",
	"page_style" => "<h1>{PHPINCE_TITLE}</h1><p>{PHPINCE_TEXT}</p>",
	"page_notfound" => "Sorry, page not found",
	"plugin_notfound" => "Plugin not found",
);
?>